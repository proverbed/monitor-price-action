1. Gather requirements 
 * What does the app need to do? 
 * What problem are you trying to solve? 
 * Get specific here

2. Describe the app 
 * Build a simple narative in plain conversational narative 
 * Uses cases, and user stories 
 * the smallest set of stories to make this a real application 

3. Identify the main objects 
 * Use stories to pick out imp ideas and concepts, things 

4. Describe the interactions 
 * Interactions between objects 
 * Behaviours/responsibilities 
 * Sequence Diagram 

5. Create a class diagram 
 * Visual representive of the specific classes you'll need

Main result is the class diagram 

Iterative approach this is not done once, but refined over weeks and months. 

Get specific about the requirements of the application. 

1. GATHERING REQUIREMENTS (Requirement Analysis)

Functional requirements: What does it do? 
 - features/capabilities 

Non-Functional Requirements: what else? 
 - Help
 - Legal 
 - Performance 
 - Support 
 - Security 

Initially what the app must do!

FURPS / FURPS+

Functional requirements 
Usability requirements 
Reliability requirements  WZ
Performance requirements
Supportability requirements

Only what is required. Agile itterative approach. To be determined? not applicable? 

Requirements 

* System need to retrieve price information on weekdays 
* System need to be able to scale 
* System needs to save price info to a database 
* System needs to 



# Design Process

What the app must do. How does a user accomplish a particular goal in the application. 

A typical user should be able to understand this. 

# 1. Use Cases

Consists of the following: 
1. Title: What is the goal? 
2. Actor: Who desires it? 
3. Scenario: How is it accomplished? 

## 1. Title: What is the goal?
Short phrase, active verb. 

e.g: 
 * Register new mmeber 
 * Transfer funds 
 * Purchase item 
 * Create new page 
 * Collect late payment

## 2. Actor: Who desires it?
Who is having this interaction? an application? another computer system? external entity. User, system admin

## 3. Scenario: how is it accomplished? 

e.g:
Customer reviews item in shopping cart. Customer provides payment and shipping information. System validates payment 
information and responds with confirmation of order and provides order number that Customer can use to check on order
status. System will send Customer a confirmation of order details and tracking number in an email. 

e.g: Scenario as numbered steps 

1. Customer chooses to enter the checkout process
2. Customer is shown a confirmation page of their order, allowing them to change quantities, remove items, or cancel. 
3. Customer enters his/her shipping address. 
4. System validates the address.
5. Customer selects a payment method. 
6. System validates the payment details. 
7. System creates an order number that can be used for tracking
8. System displays a confirmation screen to the Customer
9. Email is sent to Customer with order details. 

Scenario extentions and preconditions can also be added. 

Precondition: Customer has added atleast one item to the shopping cart. 
Extension: Describe steps for out of stock situations

### Diagram for a use case 

Its a diagram of several use cases, and multiple actors at the same time. So we can get an overview of these and see how they 
interact in context. 

This is just a diff perspective. 


2. User Stories 
