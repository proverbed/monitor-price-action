#!/bin/bash

VERSION=$1

if [ -z "$VERSION" ]
then
	echo "\$VERSION is empty, please set it to continue."
	exit 1
else
	echo "Build loadable version:[$VERSION] - generating loadable: save-priceaction-monitor-price-action-$VERSION"
	docker build --file Dockerfile -t save-priceaction-monitor-price-action-$VERSION .
fi
