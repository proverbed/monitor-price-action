package monitorpriceaction;

import java.lang.System;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.TimeZone;

import com.fxcore2.*;
import common.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

import java.io.StringWriter;

public class Main {

    public static final int HOURS = 0;
    public static final int MINUTES = 1;
    public static final String DATABASE_DATETIME_FORMAT_WITH_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ss'.000Z'";

    static int[][] marketStartTimes = {
            { 21, 00 },// Sunday
            { 00, 00 },// Monday
            { 00, 00 },// Tuesday
            { 00, 00 },// Wednesday
            { 00, 00 },// Thursday
            { 00, 00 },// Friday
    };

    static int[][] marketEndTimes = {
            { 23, 55 },// Sunday
            { 23, 55 },// Monday
            { 23, 55 },// Tuesday
            { 23, 55 },// Wednesday
            { 23, 55 },// Thursday
            { 20, 55 } // Friday
    };

    static SimpleDateFormat mDateFormat = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss zzz");
    
    public static void main(String[] args) {

        O2GSession session = null;
        try {

            String sProcName = "MonitorPriceAction";

            LoginParams loginParams = new LoginParams(args);
            SampleParams sampleParams = new SampleParams(args);
            printSampleParams(sProcName, loginParams, sampleParams);
            checkObligatoryParams(loginParams, sampleParams);

            session = O2GTransport.createSession();
            SessionStatusListener statusListener = new SessionStatusListener(session, loginParams.getSessionID(), loginParams.getPin());
            session.subscribeSessionStatus(statusListener);
            statusListener.reset();
            session.login(loginParams.getLogin(), loginParams.getPassword(), loginParams.getURL(), loginParams.getConnection());
            if (statusListener.waitEvents() && statusListener.isConnected()) {
                ResponseListener responseListener = new ResponseListener();
                session.subscribeResponse(responseListener);

                // First query the db for latest entry for today
                // if anything found, use this to query
                // otherwise use current date
                Date latestCurrentDate = getLatestCurrentDate(sampleParams);

                while( true ) {

                    Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

                    int day_of_week = c.get(Calendar.DAY_OF_WEEK);
                    int minute_of_day = c.get(Calendar.MINUTE);
                    int hour_of_day = c.get(Calendar.HOUR_OF_DAY);

                    // Sunday to Friday
                    // Hours are within the market hours
                    // Minutes are within the market minutes
                    if (
                        ( day_of_week > 0 && day_of_week < 7 )
                    ) {
                        Date startDate = createDateFromHourMinute(marketStartTimes[day_of_week - 1][HOURS], marketStartTimes[day_of_week - 1][MINUTES]);
                        Date endDate = createDateFromHourMinute(marketEndTimes[day_of_week - 1][HOURS], marketEndTimes[day_of_week - 1][MINUTES]);
                        Date currentDate = new Date();

                        if ( currentDate.before(endDate) && currentDate.after(startDate) ) {

                            System.out.println("Current Time: " + hour_of_day + ":" + minute_of_day);

                            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));       // get calendar instance
                            Date queryLatestDate;
                            if ( latestCurrentDate == null ) {
                                System.out.println( "There is no bar details for this instrument and timeframe" );

                                latestCurrentDate = new Date();
                                cal.setTime(latestCurrentDate);                        // set cal to date
                                cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
                                cal.set(Calendar.MINUTE, 0);                 // set minute in hour
                                cal.set(Calendar.SECOND, 0);                 // set second in minute
                                latestCurrentDate = cal.getTime();
                                queryLatestDate = cal.getTime();
                            } else {
                                cal.setTime(latestCurrentDate);                        // set cal to date
                                cal.add(Calendar.MINUTE, 5);                 // set second in minute
                                queryLatestDate = cal.getTime();
                            }

                            cal.setTime(new Date());                        // set cal to date
                            cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
                            cal.set(Calendar.MINUTE, 0);                 // set minute in hour
                            cal.set(Calendar.SECOND, 0);                 // set second in minute
                            cal.add(Calendar.DATE, 1);

                            Date tomorrowAfter = cal.getTime();

                            System.out.println("Database Latest DateTime: " + mDateFormat.format( latestCurrentDate ));

                            // CHANGE CURRENT TIME TO BE THE CURRENT TIME + THE INTERVAL
                            Date currentTime = createDateFromHourMinute(hour_of_day, minute_of_day);

                            Calendar newcal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                            newcal.setTime(currentTime);
                            newcal.add(Calendar.MINUTE, -5); // this is hard coded for now, this need to be abstracted, where each timeframe has a class configured
                            Date currentTimePlusInterval = newcal.getTime();

                            if (
                                ( DateGetHourOfDay(latestCurrentDate) == hour_of_day && DateGetMinuteOfDay(latestCurrentDate) == minute_of_day ) ||
                                ( currentTimePlusInterval.after(latestCurrentDate) )
                            ) {
                                getHistoryPrices(
                                    session,
                                    sampleParams.getInstrument(),
                                    sampleParams.getTimeframe(),
                                    DateToCalendar(queryLatestDate),
                                    DateToCalendar(tomorrowAfter),
                                    responseListener
                                );

                                latestCurrentDate = getLatestCurrentDate(sampleParams);
                            } else {
                                hangOn(1);
                            }

                        } else {
                            hangOn(1);
                        }
                    } else {
                        hangOn(1);
                    }
                }

            }
            session.unsubscribeSessionStatus(statusListener);

        } catch (Exception e) {
            // Send an email here

            System.out.println("Exception: " + e.toString());
        } finally {
            System.out.println("In the finally, dispose the session");

            if (session != null) {
                session.dispose();
            }
        }
    }
    
    // Request historical prices for the specified timeframe of the specified period
    public static void getHistoryPrices(O2GSession session, String sInstrument, String sTimeframe, Calendar dtFrom, Calendar dtTo, ResponseListener responseListener) throws Exception {
        O2GRequestFactory factory = session.getRequestFactory();
        O2GTimeframe timeframe = factory.getTimeFrameCollection().get(sTimeframe);
        if (timeframe == null) {
            throw new Exception(String.format("Timeframe '%s' is incorrect!", sTimeframe));
        }
        System.out.println("createMarketDataSnapshotRequestInstrument: " + sInstrument + " - " + sTimeframe );
        O2GRequest request = factory.createMarketDataSnapshotRequestInstrument(sInstrument, timeframe, 300);
        if (request == null) {
            throw new Exception("Could not create request.");
        }
        Calendar endDate = dtTo;
        Calendar dtEarliest;
        if (dtFrom == null) {
            dtEarliest = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            dtEarliest.setTime(new Date(Long.MIN_VALUE));
        } else {
            dtEarliest = dtFrom;
        }

        SimpleDateFormat mDateFormat = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss zzz");

        System.out.println("From Date: " + mDateFormat.format( dtEarliest.getTime()));
        System.out.println("To Date: " + mDateFormat.format( endDate.getTime()));

        do { // cause there is limit for returned candles amount

            factory.fillMarketDataSnapshotRequestTime(request, dtFrom, endDate, false, O2GCandleOpenPriceMode.PREVIOUS_CLOSE);
            responseListener.setRequestID(request.getRequestId());
            session.sendRequest(request);
            if (!responseListener.waitEvents()) {
                throw new Exception("Response waiting timeout expired, didn't get a response for the request, even after 30 seconds.");
            }
            // shift "to" bound to oldest datetime of returned data
            O2GResponse response = responseListener.getResponse();
            if (response != null && response.getType() == O2GResponseType.MARKET_DATA_SNAPSHOT) {
                O2GResponseReaderFactory readerFactory = session.getResponseReaderFactory();
                if (readerFactory != null) {
                    O2GMarketDataSnapshotResponseReader reader = readerFactory.createMarketDataSnapshotReader(response);
                    if (reader.size() > 0) {
                        if (endDate == null || endDate.compareTo(reader.getDate(0)) != 0) {
                            endDate = reader.getDate(0); // earliest datetime of returned data
                            SimpleDateFormat myDateformat = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss zzz");
                            System.out.println("the end date is updating: "+ endDate);
                        } else {
                            break;
                        }
                    } else {
                        System.out.println("0 rows received for the from and to dates");
                        break;
                    }
                }
                printPrices(sInstrument, sTimeframe, session, response);
            } else {
                
                break;
            }

        } while (endDate.after(dtEarliest));
    }

    public static void hangOn(int minutes) {

        try {
            Thread.sleep(minutes *   // minutes to sleep
                    60 *   // seconds to a minute
                    1000);// milliseconds to a second
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Date getLatestCurrentDate(SampleParams sampleParams) throws Exception {

        try {
            String isntrument = URLEncoder.encode(sampleParams.getInstrument(), "UTF-8");
            String timeframe = sampleParams.getTimeframe();
            String url = "http://harmonic-api:3010/instrument/" + isntrument + "/timeframe/" + timeframe + "/bar?latest_only=true";

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            // This means that we do not have any data for this timeframe and instrument in out database
            // In this case, lets use the current date
            if ( responseCode == 204 ) {
                // no latest date found
                return null;
            } else if ( responseCode == 200 ) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONParser parser = new JSONParser();

                try {
                    Object myObj = parser.parse(response.toString());
                    JSONArray arrayJson = (JSONArray)myObj;
                    JSONObject obj2 = (JSONObject)arrayJson.get(0);

                    SimpleDateFormat converter = new SimpleDateFormat(DATABASE_DATETIME_FORMAT_WITH_TIMEZONE);
                    converter.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date date = converter.parse(obj2.get("datetime").toString());

                    return date;
                }catch(ParseException pe){
                    throw new Exception("Parse exception received from datetime property from HARMONIC TRADER API" + pe.getMessage());
                }
            } else {
                throw new Exception("Invalid response code received from HARMONIC TRADER API");
            }
        } catch (Exception e) {
            throw new Exception("Something went wrong when trying to retrieve the latest bar from HARMONIC TRADER API" + e.getMessage());
        }
    }

    public static int getMinutesInTimeframe(String sTimeframe) {
        int returnValue = 0;
        if (sTimeframe.equals("m5")) {
            returnValue = 5;
        }
        return returnValue;
    }

    // Add point to database
    public static void addPointToDb(String sInstrument, String sTimeframe, Date datetime, double open, double close, double high, double low) throws Exception {

        try {
            String isntrument = URLEncoder.encode(sInstrument, "UTF-8");
            String url = "http://harmonic-api:3010/instrument/" + isntrument + "/timeframe/" + sTimeframe + "/bar";

            JSONObject objJson = new JSONObject();

            //creating DateFormat for converting time from local timezone to GMT
            DateFormat converter = new SimpleDateFormat(DATABASE_DATETIME_FORMAT_WITH_TIMEZONE);

            //getting GMT timezone, you can get any timezone e.g. UTC
            converter.setTimeZone(TimeZone.getTimeZone("UTC"));

            objJson.put("open",open);
            objJson.put("close",close);
            objJson.put("high",high);
            objJson.put("low",low);
            objJson.put("datetime",converter.format(datetime));

            StringWriter out = new StringWriter();
            objJson.writeJSONString(out);

            String jsonText = out.toString();

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(jsonText);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();

        } catch (Exception e) {
            System.out.println("addPointToDb throw some Exception: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }

    }

    // Print history data from response
    public static void printPrices(String sInstrument, String sTimeframe, O2GSession session, O2GResponse response) throws Exception {
        System.out.println(String.format("Request with RequestID=%s is completed:", response.getRequestId()));
        O2GResponseReaderFactory factory = session.getResponseReaderFactory();
        if (factory != null) {
            O2GMarketDataSnapshotResponseReader reader = factory.createMarketDataSnapshotReader(response);
            for (int ii = 0; ii <= reader.size() - 1; ii++) {
                if (reader.isBar()) {

                    Calendar cal = reader.getDate(ii);
                    cal.setTimeZone(TimeZone.getTimeZone("UTC"));

                    // add to database
                    addPointToDb(
                        sInstrument,
                        sTimeframe,
                        cal.getTime(),
                        reader.getBidOpen(ii),
                        reader.getBidClose(ii),
                        reader.getBidHigh(ii),
                        reader.getBidLow(ii)
                    );

                    System.out.println(String.format("DateTime=%s, BidOpen=%s, BidHigh=%s, BidLow=%s, BidClose=%s, AskOpen=%s, AskHigh=%s, AskLow=%s, AskClose=%s, Volume=%s",
                            mDateFormat.format(reader.getDate(ii).getTime()), reader.getBidOpen(ii), reader.getBidHigh(ii), reader.getBidLow(ii), reader.getBidClose(ii),
                            reader.getAskOpen(ii), reader.getAskHigh(ii), reader.getAskLow(ii), reader.getAskClose(ii), reader.getVolume(ii)));
                } else {
                    System.out.println(String.format("DateTime=%s, Bid=%s, Ask=%s", mDateFormat.format(reader.getDate(ii).getTime()), reader.getBidClose(ii), reader.getAskClose(ii)));
                }
            }
        }
    }
    
    // Check obligatory login parameters and sample parameters
    private static void checkObligatoryParams(LoginParams loginParams, SampleParams sampleParams) throws Exception {
        if(loginParams.getLogin().isEmpty()) {
            throw new Exception(LoginParams.LOGIN_NOT_SPECIFIED);
        }
        if(loginParams.getPassword().isEmpty()) {
            throw new Exception(LoginParams.PASSWORD_NOT_SPECIFIED);
        }
        if(loginParams.getURL().isEmpty()) {
            throw new Exception(LoginParams.URL_NOT_SPECIFIED);
        }
        if(loginParams.getConnection().isEmpty()) {
            throw new Exception(LoginParams.CONNECTION_NOT_SPECIFIED);
        }
        if(sampleParams.getInstrument().isEmpty()) {
            throw new Exception(SampleParams.INSTRUMENT_NOT_SPECIFIED);
        }
        if(sampleParams.getTimeframe().isEmpty()) {
            throw new Exception(SampleParams.TIMEFRAME_NOT_SPECIFIED);
        }

        boolean bIsDateFromNotSpecified = false;
        boolean bIsDateToNotSpecified = false;
        Calendar dtFrom = sampleParams.getDateFrom();
        Calendar dtTo = sampleParams.getDateTo();
        if (dtFrom == null) {
            bIsDateFromNotSpecified = true;
        } else {
            if(!dtFrom.before(Calendar.getInstance(TimeZone.getTimeZone("UTC")))) {
                throw new Exception(String.format("\"DateFrom\" value %s is invalid", dtFrom));
            }
        }
        if (dtTo == null) {
            bIsDateToNotSpecified = true;
        } else {
            if(!bIsDateFromNotSpecified && !dtFrom.before(dtTo)) {
                throw new Exception(String.format("\"DateTo\" value %s is invalid", dtTo));
            }
        }
    }

    // Print process name and sample parameters
    private static void printSampleParams(String procName,
            LoginParams loginPrm, SampleParams prm) {
        System.out.println(String.format("Running %s with arguments:", procName));
        if (loginPrm != null) {
            System.out.println(String.format("%s * %s %s %s %s", loginPrm.getLogin(), loginPrm.getURL(),
                  loginPrm.getConnection(), loginPrm.getSessionID(), loginPrm.getPin()));
        }
        if (prm != null) {
            System.out.println(String.format("Instrument='%s', Timeframe='%s', DateFrom='%s', DateTo='%s'",
                    prm.getInstrument(), prm.getTimeframe(),
                    prm.getDateFrom() == null ? "" : mDateFormat.format(prm.getDateFrom().getTime()),
                    prm.getDateTo() == null ? "" : mDateFormat.format(prm.getDateTo().getTime())));
        }
    }

    public static Calendar DateToCalendar(Date date){
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(date);
        return cal;
    }

    public static int DateGetHourOfDay(Date date) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(date);                        // set cal to date

        int minute_of_day = cal.get(Calendar.MINUTE);
        int hour_of_day = cal.get(Calendar.HOUR_OF_DAY);

        return hour_of_day;
    }

    public static int DateGetMinuteOfDay(Date date) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(date);                        // set cal to date

        int minute_of_day = cal.get(Calendar.MINUTE);

        return minute_of_day;
    }

    public static Date createDateFromHourMinute(int hour, int minute) {
        Calendar myCal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        myCal.set(Calendar.HOUR_OF_DAY, hour);
        myCal.set(Calendar.MINUTE, minute);
        myCal.set(Calendar.SECOND, 0);
        myCal.set(Calendar.MILLISECOND, 0);

        return myCal.getTime();
    }
}

