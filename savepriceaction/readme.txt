The ForexConnect API User Guide 

ForexConnect API is an API designed to trade Forex and CFD instruments. 
Any connection which is compatible with the Trading Station/Marketscope application or an older version of Order2Go API is also compatible with this API.

All APIs used by ForexConnect are asynchronous, so you will have to implement an event-driven architecture in your code. 

An event-driven architecture is a software architecture pattern that manages the behavior of production, 
detection and consumption of events as well as the responses they evoke. In this context, 
an event should be treated as some value or message that can be identified within an ongoing stream of monitored inputs, 
such as specific conditions or signals or something else. 

Event-driven architectures usually consist of event producers and event consumers. Event consumers subscribe to some event manager, 
and event producers publish to this manager. When the manager receives an event from a producer, 
it forwards this event to all registered consumers or stores the event for later forwarding. 

An event handler is a callback routine that operates asynchronously and handles inputs received into a program (events). 
In this context, an event is some meaningful element of application information from an underlying development framework, 
usually from a graphical user interface (GUI) toolkit or some kind of input routine. On the GUI side, 
for example, events include key strokes, mouse activity, action selections, or timer expirations. 
On the input side, events include opening or closing files and data streams, reading data and so on. 

Event handling is the receipt of an event at some event handler from an event producer and subsequent processes.
The processes involved in event handling include:
� Identifying where an event should be forwarded;
� Making the forward;
� Receiving the forwarded event;
� Taking some kind of appropriate action in response, such as writing to a log,
sending an error or recovery routine or sending a message;
� The event handler may ultimately forward the event to an event consumer. 

The benefit of event-driven architectures is that they enable arbitrarily large collections of consumers and producers, 
along with some number of managers, to exchange ongoing status and response information. 
They are also usually fairly responsive to events as they occur, and work well in unpredictable and asynchronous communication environments. 
