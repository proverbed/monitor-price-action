## Build monitor price action docker image


CD into this directory

```
docker build --file Dockerfile -t save-priceaction-monitor-price-action-4 .
```


## Build docker image

CD into this directory

`
docker build -t save-priceaction-test1 .
`

## Check container id for save-price-action 

docker ps 

docker run -i -t 65c2b39a357f /bin/bash

docker exec -it {save-price-action-container-id} bash

```
root@e2c24419843b:/usr/src/app/samples/java/NonTableManagerSamples/MonitorPriceAction# ant -f monitor-price-action.xml run
```

## Delete existing image [save-priceaction]

This command will force remove the image with tag `save-priceaction`, so that we can reuse the tag name.

`
docker rmi $(docker inspect --format="{{.Id}}" save-priceaction) -f
`

## Stop all docker containers and Remove all docker containers

`
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
`

db.Bar.insert({"high" : 1.33024,"datetime" : ISODate("2017-01-01T00:00:00Z"),"low" : 1.32994,"close" : 1.33,"open" :1.32998,"instrument" : "GBP/USD","timeframe" : "m5"})

## Link the harmonic api and docker compose network

docker run -t -i --link harmonic-api --net composetest_default {container-id} /bin/bash

## Build loadable 

bash build_loadable.sh 4